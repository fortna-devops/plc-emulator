#!/usr/bin/env python3

import os
import sys
import signal
import logging

import configargparse

import yaml

from emulator import Emulator
from tags import parse_tags
from utils import disable_cpppo_log


logger = logging.getLogger()


DEFAULT_ADDRESS = ('0.0.0.0', 44818)
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIGS_DIR = os.path.join(BASE_DIR, 'configs')
disable_cpppo_log()


def main():
    parser = configargparse.ArgumentParser(default_config_files=['config.ini'])
    parser.add_argument('-c', '--config', required=True,
                        help='path to config file')

    parser.add_argument('-a', '--address', default="%s:%d" % DEFAULT_ADDRESS,
                        help="EtherNet/IP interface[:port] to bind to "
                             "(default: %s:%d)" % DEFAULT_ADDRESS)

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level",
                           const=logging.DEBUG,
                           default=logging.WARNING)
    log_group.add_argument('-v', '--verbose', help="be verbose",
                           action="store_const",
                           dest="log_level", const=logging.INFO)

    args = parser.parse_args()

    logging.basicConfig(
        level=args.log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    logger.info('Looking for config "%s"', args.config)
    with open(args.config) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    logger.info('Available tag names "%s"', ', '.join([t['name'] for t in config['tags']]))
    logger.info('Route path "%s"', config['route_path'])

    bind = args.address.split(':')
    assert 1 <= len(bind) <= 2, "Invalid --address [<interface>]:[<port>}: %s" % args.address
    host = str(bind[0]) if bind[0] else DEFAULT_ADDRESS[0]
    port = int(bind[1]) if len(bind) > 1 and bind[1] else DEFAULT_ADDRESS[1]
    bind = (host, port)

    tags = parse_tags(config['tags'])
    emulator = Emulator(tags, bind, config['route_path'])

    def stop(signum, frame):
        logging.info('Got signal %d', signum)
        emulator.stop()
    signal.signal(signal.SIGTERM, stop)

    return emulator.start()


if __name__ == '__main__':
    sys.exit(main())
