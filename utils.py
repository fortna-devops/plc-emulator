import logging


CPPPO_LOGGERS = ['enip', 'cpppo', 'network']


__all__ = ['disable_cpppo_log']


def disable_cpppo_log():
    for logger_name in CPPPO_LOGGERS:
        logging.getLogger(logger_name).setLevel(logging.WARNING)
