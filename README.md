# PLC emulation tool

## Running
```bash
./emulate-plc.py -c ./configs/fedex-louisville.yml
```

## For more information run
```bash
./emulate-plc.py --help
```


## Tests
```bash
tox
```
