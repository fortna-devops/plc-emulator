from contextlib import contextmanager
import threading

import pytest

from emulator import Emulator
from tags import CSVTag, Tag

from .read_plc import read_csv_tag, read_tag


@pytest.fixture()
def route_path():
    return [{'link': 0, 'port': 1}]


@contextmanager
def plc_emulation(**kwargs):
    emulator = Emulator(**kwargs)
    emulation_thread = threading.Thread(target=emulator.start, name='Emulator')
    emulation_thread.daemon = True
    try:
        emulation_thread.start()
        yield
    finally:
        emulator.stop()
        emulation_thread.join()


@pytest.mark.filterwarnings('ignore')
def test_tag(route_path):
    address = ('127.0.0.1', 44818)

    tag_name = 'TAG'
    data = 42
    tag = Tag(tag_name, data)

    with plc_emulation(tags=[tag], bind=address, route_path=route_path):
        plc_data = read_tag(address, route_path, tag_name)
        assert plc_data == [data]


@pytest.mark.filterwarnings('ignore')
def test_csv_tag(route_path):
    address = ('127.0.0.1', 44818)

    tag_name = 'CSV_TAG'
    data = list(range(20))
    tag = CSVTag(tag_name, data)

    with plc_emulation(tags=[tag], bind=address, route_path=route_path):
        plc_data = read_csv_tag(address, route_path, tag_name)
        assert plc_data == ','.join([str(i) for i in data])
