from cpppo.server.enip import client


__all__ = ['read_csv_tag', 'read_tag']


_depth = 1  # Allow 1 transaction in-flight
_multiple = 0  # Don't use Multiple Service Packet
_fragment = False  # Don't force Read/Write Tag Fragmented
_timeout = 1  # Any PLC I/O fails if it takes > 1s
_printing = False  # Print a summary of I/O
_delimiter = ','  # Delimiter for PLC data string


def _get_connection(host, port):
    """ Get connection to PLC.
    :return: Connector object.
    """
    return client.connector(host, port, _timeout)


def read_csv_tag(address, route_path, tag):
    with _get_connection(*address) as connection:
        operations = client.parse_operations([tag+'.LEN'],
                                             route_path=route_path)
        failures, transactions = connection.process(
            operations=operations, depth=_depth, multiple=_multiple,
            fragment=_fragment, printing=_printing,
            timeout=_timeout
        )

        # always one transaction and len is a first byte
        data_len = transactions[0][0]

        operations = client.parse_operations(
            ['%s.DATA*%s' % (tag, data_len)], route_path=route_path)
        failures, transactions = connection.process(
            operations=operations, depth=_depth, multiple=_multiple,
            fragment=_fragment, printing=_printing,
            timeout=_timeout
        )

        return ''.join(map(chr, transactions[0]))


def read_tag(address, route_path, tag):
    with _get_connection(*address) as connection:
        operations = client.parse_operations([tag], route_path=route_path)
        failures, transactions = connection.process(
            operations=operations, depth=_depth, multiple=_multiple,
            fragment=_fragment, printing=_printing,
            timeout=_timeout
        )

        return transactions[0]
