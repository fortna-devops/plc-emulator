from cpppo.server.enip import device, parser


__all__ = ['Tag', 'CSVTag']


class Tag:

    def __init__(self, name, data=None, parser_type=parser.INT,
                 attr_cls=device.Attribute):
        self._name = name
        self._data = data
        self._attr_cls = attr_cls
        self._parser_type = parser_type

    @property
    def data(self):
        return self._data

    @property
    def attributes(self):
        return (
            self._attr_cls(self._name, self._parser_type, self.data),
        )


class CSVTag(Tag):

    @property
    def attributes(self):
        data = [ord(i) for i in ','.join(map(str, self.data))]
        return (
            self._attr_cls(self._name + '.LEN', parser.INT, len(data)),
            self._attr_cls(self._name + '.DATA', parser.INT, data),
        )
