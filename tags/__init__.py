from .tags import *


TAGS_MAP = {
    'tag': Tag,
    'csv_tag': CSVTag
}


def parse_tags(tags_config):
    result = []

    for tag_config in tags_config:
        type_str = tag_config.pop('type')
        if type_str not in TAGS_MAP:
            raise TypeError('Unknown tag type "{}".'.format(type_str))

        tag_cls = TAGS_MAP[type_str]
        tag = tag_cls(**tag_config)
        result.append(tag)

    return result
