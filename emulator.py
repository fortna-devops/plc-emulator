import threading
import logging

import cpppo
from cpppo.server import network
from cpppo.server.enip import device, logix
from cpppo.server.enip.main import enip_srv


logger = logging.getLogger()


class Emulator(object):

    def __init__(self, tags, bind, route_path):
        self._event = threading.Event()
        self._tags = tags
        self._bind = bind
        self._route_path = route_path

        self._srv_ctl = cpppo.dotdict()
        self._srv_ctl.control = cpppo.dotdict()
        self._srv_ctl.control.disable = False
        self._srv_ctl.control.done = False

    def _get_options(self):
        class UCMM(device.UCMM):
            route_path = self._route_path

        options = cpppo.dotdict(
            UCMM_class=UCMM,
            enip_process=logix.process,
            tags=self._get_tags(),
            server=self._srv_ctl
        )

        return options

    def _get_tags(self):
        tags = cpppo.dotdict()
        for tag in self._tags:
            for attribute in tag.attributes:
                tag_entry = cpppo.dotdict()
                tag_entry.attribute = attribute
                tag_entry.path = None
                tag_entry.error = 0x00
                dict.__setitem__(tags, attribute.name, tag_entry)

        return tags

    def start(self):
        logger.info('Starting PLC emulator')
        return network.server_main(address=self._bind, target=enip_srv,
                                   kwargs=self._get_options())

    def stop(self):
        logger.info('Stopping PLC emulator')
        self._srv_ctl.control.done = True
